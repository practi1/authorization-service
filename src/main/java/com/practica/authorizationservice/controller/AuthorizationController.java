package com.practica.authorizationservice.controller;

import javax.validation.Valid;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practica.authorizationservice.dto.entrada.ICredencialDto;
import com.practica.commonsmodule.error.FaltaArgumento;
import com.practica.commonsmodule.utilis.Convert;

@RestController
@RequestMapping("/authorization")
public class AuthorizationController {
	
	@Value("${keycloak.url}")
    private String serverUrl;
	@Value("${keycloak.realm}")
	private String realm;
	@Value("${keycloak.clientId}")
	private String clientId;
	@Value("${keycloak.secret}")
	private String secret;
	
	@PostMapping("/login")
    public AccessTokenResponse getToken(@Valid @RequestBody ICredencialDto iCredencialDto, BindingResult bindingResult) { 
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
	    
	    var keycloak = Keycloak.getInstance(
	    		serverUrl,
	    		realm,
	    		iCredencialDto.getUsuario(),
	    		iCredencialDto.getContrasena(),
	    		clientId,
	    		secret);
	    
	    return keycloak.tokenManager().getAccessToken();

    }

}
