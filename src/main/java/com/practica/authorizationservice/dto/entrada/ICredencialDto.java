package com.practica.authorizationservice.dto.entrada;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ICredencialDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	@NotEmpty(message = "Usuario no puede ser vacio o nulo")
	private String usuario;
	@NotEmpty(message = "Contraseña no puede ser vacio o nulo")
	private String contrasena;

}
